# -*- coding: utf-8 -*-
from django.shortcuts import render

from app_test.models import Theme


def popular_themes(request):
    themes = sorted(Theme.objects.all(), key=lambda theme: theme.score)
    return render(request, 'index.html', {'themes': themes})
