# Test Project:
Project used to test backend skills.

## Without Docker

### BACKEND
#### Configuration:

1. Fork the project on GitLab and clone it to your local respository:

        git clone https://eliosf27@bitbucket.org/eliosf27/backendtest.git && cd backendtest

2. Config python enviroment:

Choose only one option to work with python enviroment
    
a)  Install dependecies:
        
        sudo apt install python-pip
        sudo pip install virtualenv

b) Config Virtualenv (Optional):

        virtualenv -p python3 backendtest_env
        source backendtest_env/bin/activate

c) Install the requirements:

         pip install -r requirements.txt

d) Run development server:

        python backend/manage.py runserver

### Tasks
* Run tests:

        cd backend ; python manage.py test

* Run PEP8 validations:

        flake8

* Verify if the imports are correctly sorted

        isort -c -rc -df

Note: We can run all the tests and the validations with a single command:
        
        chmod +x test-all.sh
        ./test-all.sh

## With Docker
0. Install and configure Docker

    * Install docker. [Docker](https://www.docker.com)

    * Install docker-compose. [Compose](https://docs.docker.com/compose/install/)

## Instructions:
1. Clone the repo:  
   
   `git clone https://eliosf27@bitbucket.org/eliosf27/backendtest.git`

2. Build the docker image:  
   
   `cd backendtest/ && docker-compose build`

3. Starts the containers in the background and leaves them running.:
   
   `docker-compose up -d`  

4. Open the following URL:
   
   [http://localhost:8080/](http://localhost:8080/)

5. Run backend tests:
   
   `docker-compose exec django bash test-all.sh` 
   

